﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Progress_assignment.Models
{
    public class FullCustomerModel
    {
        public string FullName { get; set; }
        public int Age { get; set; }
        public string BirthPlace { get; set; }
    }
}
